provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_ecs_cluster" "cluster" {
  name               = "terraform-ecs-cluster"
  capacity_providers = ["FARGATE_SPOT", "FARGATE"]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
  setting {
    name  = "containerInsights"
    value = "disabled"
  }
}
module "ecs-fargate" {
  source                          = "umotif-public/ecs-fargate/aws"
  version                         = "~> 5.1.0"
  name_prefix                     = "ecs-fargate-example"
  container_name                  = "ecs-fargate-example2"
  vpc_id                          = "vpc-05a9e3389b85681f1"
  private_subnet_ids              = ["subnet-0362a6b27c739b179"]
  cluster_id                      = aws_ecs_cluster.cluster.id
  task_container_image            = "704361135933.dkr.ecr.ap-southeast-1.amazonaws.com/test:latest"
  desired_count                   = 1
  task_definition_cpu             = 256
  task_definition_memory          = 512
  task_container_port             = 80
  task_container_assign_public_ip = true
  lb_arn                          = "arn:aws:elasticloadbalancing:ap-southeast-1:704361135933:loadbalancer/app/test-alb/b839e2cb092f7b06"
  health_check = {
    port = "traffic-port"
    path = "/"
  }
  health_check_grace_period_seconds = 3000
  tags = {
    Environment = "test"
    Project     = "Test"
  }
  #   name            = "nginxservice"
  #   cluster         = module.ecs_cluster.aws_ecs_cluster.cluster_name
  #   task_definition = aws_ecs_task_definition.mongo.arn
  #   desired_count   = 3
  #   iam_role        = aws_iam_role.foo.arn
  #   depends_on      = [aws_iam_role_policy.foo]
  #   launch_tpye     = "FARGATE"
}
