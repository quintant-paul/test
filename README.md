# Task 1: Terraform, ECS, Cloudwatch Logs
We want to host Wordpress website on ECS. Which will be highly available and scalable.
Setup Nginx Containers as reverse proxy on ECS.
Send Host and Container Logs to Cloudwatch Logs
Need logical solution to automate deployment of application update on ECS.
If you can terraformize it then more preferred.

Delivery:
Write terraform code for above assignment with reusable modular approach. 
Provide architecture diagram with it.
Note: You can use ECS / Nomad or anyother container service provider.


# Task 2: ChatOps or Runbook automation
We want to automate a process where oncall gets an alert, and then can open a dashboard to run a script (or even to just run a command in slack using chatops), what would you recommend? Please describe the different options you would recommend for runbook and task automation. The particular focus is how to automate common runbook tasks so they can be done at a click of a button by someone oncall. And it should be usable by someone on a mobile phone, and not only via a laptop. And please give a few simple examples of how your proposed solution(s) would work. 
 
 

## Task 1:
Manual (Testing without Terraform)
build nginx and wordpress image and run container on an EC2 instances with docker. 

## Running image for wordpress 
1.	Run a container from dockerhub repository
map port 80 of the container to 8000 of the host
```bash
docker run --name some-wordpress-paul -p 8000:80 -d wordpress
 ```
 ![alt text](https://gitlab.com/quintant-paul/test/-/blob/master/images/1.png)
 ![Image](../blob/master/images/1.png)

## Building image for nginx 
1.	Modify “proxy_pass” parameter to wordpress container running in the same host by editing /etc/nginx/conf.d/default.conf
  
 
2.	Create a dockerfile to build a image
```bash
docker build -t nginxtestpaul .
```

 
3.	Confirm the default.conf is as per dockerfile 
```bash
docker exec -it 2e58c44e399d /bin/bash
cat /etc/nginx/conf.d/default.conf
``` 
 
4.	Review nginx is doing reverse proxy correctly over internet
(configure an A record of “test” in DNS) 
 
http://test.uctechdemo.com/ 
(default port is 80, can be review by anyone who wish to verify)
 

 
## Setting up ECS 
Pushing image to an image registry
1.	Tag the image and push it to an image repository
```bash
docker container commit 242214b70026 nginxtestpaul:latest
docker image tag nginxtestpaul:latest paulquintant/towkayfrontend:nginxtestpaul
``` 


2.	Push the image to an image registry 
(I have a private repository in ecr repo test, so I used it)
```bash
docker push 704361135933.dkr.ecr.ap-southeast-1.amazonaws.com/test:latest
``` 
 
 
Create ECS 
3.	Create ECS cluster(fargate)
 


4.	Create ECS Service
 
5.	Going to ECS to create task to run a container using the image create in my private repo earlier on in ECR

   
 
 
6.	Verify Task is running fine. 
 
 
http://54.151.142.36  
(For verification purpose. Not static IP but a dynamic Pub IP. As of 1st Feb still running)
 
 
## Using Terraform 
1.	Create ECS Cluster & Service  & Task Definition 
What am I automating: only use Terraform for the initial provisioning and when we use new services. When Terraform initially creates the service & cluster with a dummy task definition that uses placeholders until an actual deployment takes place. 
*ALB is created manually*


```terraform
provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_ecs_cluster" "cluster" {
  name               = "terraform-ecs-cluster"
  capacity_providers = ["FARGATE_SPOT", "FARGATE"]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
  setting {
    name  = "containerInsights"
    value = "disabled"
  }
}
module "ecs-fargate" {
  source                          = "umotif-public/ecs-fargate/aws"
  version                         = "~> 5.1.0"
  name_prefix                     = "ecs-fargate-example"
  container_name                  = "ecs-fargate-example2"
  vpc_id                          = "vpc-05a9e3389b85681f1"
  private_subnet_ids              = ["subnet-0362a6b27c739b179"]
  cluster_id                      = aws_ecs_cluster.cluster.id
  task_container_image            = "704361135933.dkr.ecr.ap-southeast-1.amazonaws.com/test:latest"
  desired_count                   = 1
  task_definition_cpu             = 256
  task_definition_memory          = 512
  task_container_port             = 80
  task_container_assign_public_ip = true
  lb_arn                          = "arn:aws:elasticloadbalancing:ap-southeast-1:704361135933:loadbalancer/app/test-alb/b839e2cb092f7b06"
  health_check = {
    port = "traffic-port"
    path = "/"
  }
  health_check_grace_period_seconds = 3000
  tags = {
    Environment = "test"
    Project     = "Test"
  }
```

## Result
```bash
terraform apply
module.ecs-fargate.aws_cloudwatch_log_group.main: Refreshing state... [id=ecs-fargate-example]
module.ecs-fargate.aws_security_group.ecs_service: Refreshing state... [id=sg-0ce4546d61686b14f]
aws_ecs_cluster.cluster: Refreshing state... [id=arn:aws:ecs:ap-southeast-1:704361135933:cluster/terraform-ecs-cluster]
module.ecs-fargate.aws_lb_target_group.task[0]: Refreshing state... [id=arn:aws:elasticloadbalancing:ap-southeast-1:704361135933:targetgroup/ecs-fargate-example-target-80/911ab81efbad422e]
module.ecs-fargate.aws_iam_role.task: Refreshing state... [id=ecs-fargate-example-task-role]
module.ecs-fargate.aws_iam_role.execution: Refreshing state... [id=ecs-fargate-example-task-execution-role]
module.ecs-fargate.aws_security_group_rule.egress_service: Refreshing state... [id=sgrule-150471554]
module.ecs-fargate.aws_iam_role_policy.log_agent: Refreshing state... [id=ecs-fargate-example-task-role:ecs-fargate-example-log-permissions]
module.ecs-fargate.aws_iam_role_policy.task_execution: Refreshing state... [id=ecs-fargate-example-task-execution-role:ecs-fargate-example-task-execution]
module.ecs-fargate.aws_ecs_task_definition.task: Refreshing state... [id=ecs-fargate-example]

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_ecs_cluster.cluster will be created
  + resource "aws_ecs_cluster" "cluster" {
      + arn                = (known after apply)
      + capacity_providers = [
          + "FARGATE",
          + "FARGATE_SPOT",
        ]
      + id                 = (known after apply)
      + name               = "terraform-ecs-cluster"

      + default_capacity_provider_strategy {
          + capacity_provider = "FARGATE_SPOT"
        }

      + setting {
          + name  = "containerInsights"
          + value = "disabled"
        }
    }

  # module.ecs-fargate.aws_ecs_service.service will be created
  + resource "aws_ecs_service" "service" {
      + cluster                            = (known after apply)
      + deployment_maximum_percent         = 200
      + deployment_minimum_healthy_percent = 50
      + desired_count                      = 1
      + enable_ecs_managed_tags            = false
      + force_new_deployment               = false
      + health_check_grace_period_seconds  = 3000
      + iam_role                           = (known after apply)
      + id                                 = (known after apply)
      + launch_type                        = "FARGATE"
      + name                               = "ecs-fargate-example"
      + platform_version                   = "LATEST"
      + propagate_tags                     = "TASK_DEFINITION"
      + scheduling_strategy                = "REPLICA"
      + tags                               = {
          + "Environment" = "test"
          + "Name"        = "ecs-fargate-example-service"
          + "Project"     = "Test"
        }
      + task_definition                    = "arn:aws:ecs:ap-southeast-1:704361135933:task-definition/ecs-fargate-example:1"
      + wait_for_steady_state              = false

      + deployment_controller {
          + type = "ECS"
        }

      + load_balancer {
          + container_name   = "ecs-fargate-example2"
          + container_port   = 80
          + target_group_arn = "arn:aws:elasticloadbalancing:ap-southeast-1:704361135933:targetgroup/ecs-fargate-example-target-80/911ab81efbad422e"
        }

      + network_configuration {
          + assign_public_ip = true
          + security_groups  = [
              + "sg-0ce4546d61686b14f",
            ]
          + subnets          = [
              + "subnet-0362a6b27c739b179",
            ]
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_ecs_cluster.cluster: Creating...
aws_ecs_cluster.cluster: Still creating... [10s elapsed]
aws_ecs_cluster.cluster: Creation complete after 10s [id=arn:aws:ecs:ap-southeast-1:704361135933:cluster/terraform-ecs-cluster]
module.ecs-fargate.aws_ecs_service.service: Creating...
module.ecs-fargate.aws_ecs_service.service: Creation complete after 1s [id=arn:aws:ecs:ap-southeast-1:704361135933:service/terraform-ecs-cluster/ecs-fargate-example]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```

aws ecs list cluster
```
$ aws ecs list-clusters
{
    "clusterArns": [
        "arn:aws:ecs:ap-southeast-1:704361135933:cluster/quintant-dev-fargate",
        "arn:aws:ecs:ap-southeast-1:704361135933:cluster/terraform-ecs-cluster",
        "arn:aws:ecs:ap-southeast-1:704361135933:cluster/talc-dev",
        "arn:aws:ecs:ap-southeast-1:704361135933:cluster/test"
    ]
}
```

aws ecs describe --cluster terraform-ecs-cluster
```
$ aws ecs describe-clusters --clusters terraform-ecs-cluster
{
    "clusters": [
        {
            "clusterArn": "arn:aws:ecs:ap-southeast-1:704361135933:cluster/terraform-ecs-cluster",
            "clusterName": "terraform-ecs-cluster",
            "status": "ACTIVE",
            "registeredContainerInstancesCount": 0,
            "runningTasksCount": 1,
            "pendingTasksCount": 0,
            "activeServicesCount": 1,
            "statistics": [],
            "tags": [],
            "settings": [
                {
                    "name": "containerInsights",
                    "value": "disabled"
                }
            ],
            "capacityProviders": [
                "FARGATE_SPOT",
                "FARGATE"
            ],
            "defaultCapacityProviderStrategy": [
                {
                    "capacityProvider": "FARGATE_SPOT",
                    "weight": 0,
                    "base": 0
                }
            ]
        }
    ],
    "failures": []
}
```


 
# Challenges faced on Task1
1.	How nginx do reverse_proxy with high availability

I am not be fully aware of how to properly configure a HA nginx 
Still attempting. Work around, just run 1 ECS task with nginx image. 

2.	Unable to pull Wordpress image from hub.docker in ECS

Still attempting to set up a proper AWS Secret manager to login Hub.Docker 
I do a workaround by running the wordpress from my EC2 instance and proxy_pass to my EC2 public IP:80. 

3.	Limited resource/budget to do the test for High Availability 
a.	not creating multiple task in ECS

but if according to planning, will either create an ELB / run nginx cluster in public subnet in VPC.
if using ELB, will use Health check setting to automate check and restart the ECS tasks. (Example of how I do health check for my test project in ALB)
 
and run 2 task count for wordpress container in private subnet in VPC
will include aws auto scaling policy 

b.	Create an actual public and private subnet but did not fully utilize

have issue pulling wordpress container.

c.	Not buying EIP, ELB

Partly, for demo purpose, I wish to reduce the cost. 

4.	Not sure the best reusable way of writing terraform

I am not familiar with terraform at this moment, but will pick them up independently when I have a practical direction. I need actual practical use case to understand what most repeatable task to automate and work on with example. Base on my current exposure in testing, I will assume spinning up ECS cluster and services as infra-as-a-code, provision out an environment to host app at a quicker pace w/o human-prone error. 



# Task 2:
User problem statement: 
How to automate common runbook tasks so they can be done at a click of a button by someone oncall. And it should be usable by someone on a mobile phone, and not only via a laptop


## Proposal 1
IBM Resilent

Assumption: 

•	oncall gets an alert

o	assume oncall is referring to someone on duty. 

o	He gets alerts from email

o	Alert is usually regarding infra operating issues (e.g. security alert)

•	runbook and task automation

o	runbook = set of tasks he has to complete, each has to submit an artefact to prove he completed the task at the end of each task


Stalk-holder: Operator, System-owner, IT-security enforcement team. 

Scenario: System owner receives security threat alert flagged but waiting for operator to perform rectification action. Alerted issue is unresolved for over service level agreement and IT security governance team has to be involved to resolve the treat together. 

Proposed solution: 
Using QRadar to perform security analytics for insight into your most critical threats, 
extend it with Resilent and define a suite of action /workflow to address different issue.
So when next threat arise, even with a non-well trained operator can act more independently for incident that already has been included. 
Problem: No mobile version. 
Workaround: But the software can be installed in EC2 instance and expose the web. 

Reference :

Resilent - 
https://www.ibm.com/security/intelligent-orchestration/soar 

QRadar - 
https://www.ibm.com/sg-en/security/security-intelligence/qradar?p1=Search&p4=43700052661478691&p5=e&cm_mmc=Search_Google-_-1S_1S-_-AS_SG-_-qradar_e&cm_mmca7=71700000065345715&cm_mmca8=aud-311016886972:kwd-295629431973&cm_mmca9=CjwKCAiAjeSABhAPEiwAqfxURejxNsq-wK50sJ5FjkMLzr_pynOaCtGr3tMLwEFnNzRjjBRpLvafoxoCU7QQAvD_BwE&cm_mmca10=427961820612&cm_mmca11=e&gclsrc=aw.ds&&gclid=CjwKCAiAjeSABhAPEiwAqfxURejxNsq-wK50sJ5FjkMLzr_pynOaCtGr3tMLwEFnNzRjjBRpLvafoxoCU7QQAvD_BwE 

 
## Proposal 2
Octopus 

Assumption: 

•	oncall gets an alert

o	assume oncall is referring to someone on duty. 

o	He gets alerts from email

o	Alert is usually application downtime

•	runbook and task automation

o	runbook = documents or wiki pages describing IT processes that keep your applications running smoothly

Stalk-holder: Operator, System-owner.

Scenario: System owner receives notification of websites go down and infrastructure fails. Waiting for operator to perform rectification action. Operator is not well-trained and runbooks are all over the place. 

Proposed solution: Use Octopus Deploy to provide runbooks and deployments that share configuration settings, secrets, templates, scripts, and more so it’s easier to configure deployment and runbook processes.

Reference:

Octopus - https://octopus.com/runbooks 

## Proposal 3
Telegram bot with Jira Service Desk

Assumption: 

•	oncall gets an alert

o	assume oncall is referring to someone on duty. 

o	He gets alerts from email

o	Alert is usually task dateline is drawing close. 

•	runbook and task automation

o	runbook = documents or wiki pages describing IT processes that keep your applications running smoothly

Stalk-holder: Developer, Project manager, Product owner.

Scenario: Need to create a service that provides quick and convenient answers to issue reporters.

Proposed solution: Telegram Integration for Jira allows you to connect your Telegram account with Jira to get instant notifications about issues and interact with your team in a more convenient way. It allows you to post and resolve an issue all in one chat, posts messages from Telegram chat to linked issue in Jira, helps users receive notifications about issue status from the Telegram bot, etc. It also allows the user to input customer service feedback once the issue has been closed. 

Reference:

Octopus - https://ronhul.xyz/jira-telegram-notifications/#Due_Date_Reminders 

## Summary
Do not have enough time to perform testing for proposed solution, may not fully understand the requirement and do not fully research what is ChatOps. Given longer time, will create a demo to test if solution can fulfil the requirement. 
